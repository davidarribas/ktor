package com.example

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.gson.*
import io.ktor.features.*

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(ContentNegotiation) {
        gson {
        }
    }

    user()
}

data class User(var id: Long, val name: String, val surname: String) {
    override fun equals(other: Any?): Boolean {
        if (other is User) {
            return other.id == id
        }
        return super.equals(other)
    }

    override fun hashCode(): Int = id.hashCode()
}

data class Error(val message: String)
data class Succesfull(val message: String)

val users = mutableListOf<User>()

fun Application.user() {
    routing {
        route("/user") {
            get {
                when (users.isNotEmpty()) {
                    true -> call.respond(users)
                    else -> call.respond(HttpStatusCode.OK, Succesfull("User list is empty"))
                }
            }
            get("/{id}") {
                val candidate = call.parameters["id"]?.toLongOrNull()

                when (candidate) {
                    null -> call.respond(HttpStatusCode.BadRequest, Error("ID must de long"))
                    else -> {
                        val user = users.firstOrNull { it.id == candidate }
                        when (user) {
                            null -> call.respond(HttpStatusCode.BadRequest, Error("User with id $candidate not found"))
                            else -> call.respond(HttpStatusCode.OK, Succesfull(user.toString()))
                        }
                    }
                }
            }
            post {
                val candidate = call.receive<User>()
                users.add(candidate)
                call.respond(HttpStatusCode.Created, Succesfull("User create"))
            }

            put {
                val candidate = call.receive<User>()

                val result =
                    when (users.contains(candidate)) {
                        true -> {
                            users[users.indexOf(candidate)] = candidate
                            call.respond(HttpStatusCode.OK, Succesfull("User update"))
                        }
                        false -> call.respond(HttpStatusCode.NotFound, Error("User with id ${candidate.id} not found"))

                    }
            }

            delete("/{id}") {
                val candidateId = call.parameters["id"]?.toLongOrNull()
                call.respond(HttpStatusCode.OK, Succesfull("User $candidateId deleted"))

                val result = when (candidateId) {
                    null -> call.respond(HttpStatusCode.BadRequest, Error("ID must be long"))
                    else -> {
                        val user = users.firstOrNull { it.id == candidateId }
                        when (user) {
                            null -> call.respond(HttpStatusCode.NotFound, Error("User with id $candidateId not found"))
                            else -> {
                                users.remove(user)
                                call.respond(HttpStatusCode.OK, Succesfull("User $candidateId deleted"))
                            }
                        }
                    }
                }
            }
        }
    }
}

